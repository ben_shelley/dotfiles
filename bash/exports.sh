# Make vim the default editor
export EDITOR="vim"

# Don't clear the screen after quitting a manual page
export MANPAGER="less -X"

# Highlight section titles in manual pages
export LESS_TERMCAP_md="$ORANGE"

# Larger bash history (allow 32k entries; default is 500)
export HISTSIZE=32768
export HISTFILESIZE=$HISTSIZE
export HISTCONTROL=ignoredups

# Make some commands not show up in history
export HISTIGNORE="ls:cd:cd -:pwd:exit:date:* --help:[bf]g:clear:cl:history:cat *:"

cabundle="$HOME/.ca-bundle.crt"
if [ -f "$cabundle" ]; then
    export CURL_CA_BUNDLE="$cabundle"
    export REQUESTS_CA_BUNDLE="$cabundle"
    export SSL_CERT_FILE="$cabundle"
fi

# vim: set filetype=sh:
