# @gf3’s Sexy Bash Prompt, inspired by “Extravagant Zsh Prompt”
# Shamelessly copied from https://github.com/gf3/dotfiles
# Screenshot: http://i.imgur.com/s0Blh.png

if [[ $COLORTERM = gnome-* && $TERM = xterm ]] && infocmp gnome-256color >/dev/null 2>&1; then
    export TERM=gnome-256color
elif infocmp xterm-256color >/dev/null 2>&1; then
    export TERM=xterm-256color
fi

# if tput setaf 1 &> /dev/null; then
if [ -x /usr/bin/tput ] && tput setaf 1 >&/dev/null; then
    tput sgr0 # reset colors

    if [[ $(tput colors) -ge 256 ]] 2>/dev/null; then
        BASE03=$(tput setaf 234)
        BASE02=$(tput setaf 235)
        BASE01=$(tput setaf 240)
        BASE0=$(tput setaf 244)
        BASE1=$(tput setaf 245)
        BASE2=$(tput setaf 254)
        BASE3=$(tput setaf 230)
        YELLOW=$(tput setaf 136)
        ORANGE=$(tput setaf 166)
        RED=$(tput setaf 160)
        MAGENTA=$(tput setaf 125)
        VIOLET=$(tput setaf 61)
        BLUE=$(tput setaf 33)
        CYAN=$(tput setaf 37)
        GREEN=$(tput setaf 64)
    else
        BASE03=$(tput setaf 8)
        BASE02=$(tput setaf 0)
        BASE01=$(tput setaf 10)
        BASE0=$(tput setaf 11)
        BASE1=$(tput setaf 12)
        BASE2=$(tput setaf 7)
        BASE3=$(tput setaf 15)
        YELLOW=$(tput setaf 3)
        ORANGE=$(tput setaf 9)
        RED=$(tput setaf 1)
        MAGENTA=$(tput setaf 5)
        VIOLET=$(tput setaf 13)
        BLUE=$(tput setaf 4)
        CYAN=$(tput setaf 6)
        GREEN=$(tput setaf 2)
    fi
    BOLD=$(tput bold)
    RESET=$(tput sgr0)
else
    # Linux console colors
    MAGENTA="\033[1;31m"
    ORANGE="\033[1;33m"
    GREEN="\033[1;32m"
    PURPLE="\033[1;35m"
    WHITE="\033[1;37m"
    BOLD=""
    RESET="\033[m"
fi

userStyle="\[${BOLD}${GREEN}\]"

# Connected via SSH
if [[ "$SSH_TTY" ]]; then
    hostStyle="\[${BOLD}${ORANGE}\]"
else
    if [[ -n "$CYAN" ]]; then
        hostStyle="\[$CYAN\]"
    else
        hostStyle="$PURPLE"
    fi
fi

# Set the terminal title to the current working directory
PS1="\[\033]0;\w\a\]"
PS1+="\[${BOLD}${GREEN}\]\u "
PS1+="\[$BASE0\]at "
PS1+="${hostStyle}\h "
PS1+="\[$BASE0\]in "
PS1+="\[$YELLOW\]\w\[$BASE0\]"

# Git prompt
GIT_PROMPT="${HOME}/.git-prompt.sh"
if [ -f "$GIT_PROMPT" ]; then
    export GIT_PS1_SHOWDIRTYSTATE=true
    export GIT_PS1_SHOWSTASHSTATE=true
    export GIT_PS1_SHOWUNTRACKEDFILES=true
    export GIT_PS1_SHOWUPSTREAM=auto
    export GIT_PS1_DESCRIBE_STYLE=branch
    export GIT_PS1_SHOWCOLORHINTS=true
    source "$GIT_PROMPT"
    PS1+='$(__git_ps1 " (%s)")'
else
    parse_git_branch() {
        git branch 2> /dev/null | sed -e '/^[^*]/d' -e 's/* \(.*\)/ (\1)/'
    }
    PS1+='$(parse_git_branch)'
fi

# function conda_env() {
#     if [[ -n "$CONDA_DEFAULT_ENV" ]]; then
#         echo " ($CONDA_DEFAULT_ENV)"
#     else
#         echo ""
#     fi
# }
# PS1+='$(conda_env)'
PS1+="\n\[$RESET\]\$ "

export PS1
export PS2="\[$ORANGE\]→ \[$RESET\]"

# vim: set filetype=sh:
