# Create a new directory and enter it
function mkd() {
    mkdir -p "$@" && cd "$@"
}

# Remove an entry from PATH
function path_remove() { export PATH=$(echo -n $PATH | awk -v RS=: -v ORS=: '$0 != "'$1'"' | sed 's/:$//'); }

# Append to PATH
function path_append() { path_remove "${1}"; export PATH="${PATH}:${1}"; }

# Prepend to PATH
function path_prepend() { path_remove "${1}"; export PATH="${1}:${PATH}"; }

# Echo the path and split with newlines
function path() {
    echo $PATH | tr ':' '\n'
}

# Determine size of a file or total size of a directory
function fs() {
    if du -b /dev/null > /dev/null 2>&1; then
        local arg=-sbh
    else
        local arg=-sh
    fi
    if [[ -n "$@" ]]; then
        du $arg -- "$@"
    else
        du $arg .[^.]* *
    fi
}

# Compare original and gzipped file size
function gz() {
    local origsize=$(wc -c < "$1")
    local gzipsize=$(gzip -c "$1" | wc -c)
    local ratio=$(echo "$gzipsize * 100/ $origsize" | bc -l)
    printf "orig: %d bytes\n" "$origsize"
    printf "gzip: %d bytes (%2.2f%%)\n" "$gzipsize" "$ratio"
}

# View svn diff in Vim
function svndiff() {
    svn diff "${@}" | vim -RM -
}

# View hg diff in Vim
function hgdiff() {
    hg diff "${@}" | vim -RM -
}

function vidjoin() {
    mencoder -oac copy -ovc copy -idx -o output.avi "${@}"
}

function addnewlines() {
    for x in "${@}"; do if [ -n "$(tail -c 1 <"$x")" ]; then echo >>"$x"; fi; done
}

function tarit() {
    if [ $# -lt 1 ]; then
        echo "Directory to tar must be specified"
        return 1
    fi
    if [ $# -lt 2 ]; then
        output_dir="."
    else
        output_dir="${2}"
    fi
    if [ ! -d "$1" ]; then
        echo "Directory does not exist: $1"
        return 1
    fi
    local timestamp=$(date "+%Y-%m-%d_%H%M")
    local dirname=$(basename "$1")
    tar -czvf "${output_dir}/${dirname}_${timestamp}.tgz" "$1"
}

# Self extracting encrypted tarball
function tgzx() {
    (( ${#} >= 2 )) || { echo 'usage: tgzx archive-file [files | directories]'; return 1; }
    printf '$!/usr/ben/env bash\ntail -n+3 ${0} | openssl enc -aes-256-cbc -d -md sha512 -a | tar {$1:-xv}z; exit\n' >"${1}"
    tar zc "${@:2}" | openssl enc -aes-256-cbc -md sha512 -a -salt >>"${1}" && chmod a+x "${1}"
}

timestamp() {
    date "+%Y-%m-%d_%H%M%S"
}

sshcwd() {
    ssh -Y -t "$1" "cd $(pwd); bash"
}

# vim: set filetype=sh:
