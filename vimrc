" Use pathogen-vim for runtime path manipulation
execute pathogen#infect()
filetype on
filetype plugin indent on

" Automatically read when a file is changed from the outside
set autoread

" Make Vim more useful
set nocompatible

" Use the OS clipboard by default (on versions compiled with `+clipboard`)
set clipboard=unnamed

" Enhance command-line completion
set wildmenu

" Ignore compiled files
set wildignore=*.o,*~,*.pyc
if has("win16") || has("win32")
    set wildignore+=.git\*,.hg\*,.svn\*
else
    set wildignore+=*/.git/*,*/.hg/*,*/.svn/*,*/.DS_Store
endif

" Allow cursor keys in insert mode
set esckeys

" Allow backspace in insert mode
set backspace=indent,eol,start

" Optimize for fast terminal connections
set ttyfast

" Add the g flag to search/replace by default
set gdefault

" Use UTF-8 without BOM
set encoding=utf-8 nobomb

" Use unix as the default file format
set fileformats=unix,dos,mac

" Change mapleader
let mapleader=","

" Don’t add empty newlines at the end of files
" This option screws up dos/unix fileformat detection
" set binary
" set noeol

" Centralize backups, swapfiles and undo history
set backupdir=~/.vim/backups
set directory=~/.vim/swaps
if exists("&undodir")
    set undofile
    set undodir=~/.vim/undo
endif

" Respect modeline in files
" set modeline
" set modelines=4

" Enable per-directory .vimrc files and disable unsafe commands in them
set exrc
set secure

" Enable line numbers
set number

" Enable 256 color palette in Gnome Terminal
if $COLORTERM == 'gnome-terminal'
    set t_Co=256
endif

" Enable syntax highlighting and use the Solarized colorscheme
syntax enable
if has('gui_running')
    set background=light
    set guioptions-=T
    set guioptions-=e
    set t_Co=256
    set guitablabel=%M\ %t
else
    set background=dark
endif
" let g:solarized_termcolors=256
colorscheme solarized

" Highlight current line
set cursorline

" Make tabs as wide as four spaces and use spaces instead of tab characters
set tabstop=4
set shiftwidth=4
set expandtab

" Improve tabbing
set smarttab

" Highlight matching brackets in programming languages
set showmatch

" If you're indented, new lines will also be indented
set autoindent

" Automatically indents lines after opening brackets in programming languages
" (not needed since using the indent plugin?)
set smartindent

" Lets you hide sections of code
"set foldmethod=manual
set foldmethod=indent
set foldlevel=99

" Hard-wrap lines at 80 characters
" set textwidth=80

" Turn off line wrapping
set nowrap

" Set format options (see :help fo-table)
set formatoptions=qrn1

" Show a colored column to indicate lines that are too long
if has('colorcolumn')
    set colorcolumn=80
endif

" Turn on line wrapping at word boundaries
" set wrap
set linebreak

" Show “invisible” characters
set lcs=tab:▸\ ,trail:·,eol:¬,nbsp:_
set list
" set nolist

" Highlight searches
set hlsearch

" Clear highlighted search results with ,<space>
nnoremap <leader><space> :noh<cr>

" Smart handling of case-sensitivity in searches:
"    - All lower-case search string = case insensitive
"    - One or more upper-case in search string = case sensitive
set ignorecase
set smartcase

" Highlight dynamically as pattern is typed
set incsearch

" Always show status line
set laststatus=2

" Enable mouse in all modes
set mouse=a

" Disable error bells
set noerrorbells

" Don’t reset cursor to start of line when moving around.
set nostartofline

" Show the cursor position
set ruler

" Don’t show the intro message when starting Vim
set shortmess=atI

" Show the current mode
set showmode

" Show the filename in the window titlebar
set title

" Show the (partial) command as it’s being typed
set showcmd

" Hide buffers instead of closing them
set hidden

" Use relative line numbers
if exists("&relativenumber")
  set relativenumber
  au BufReadPost * set relativenumber
endif

" Start scrolling three lines before the horizontal window border
set scrolloff=3

" Strip trailing whitespace (,ss)
function! StripWhitespace()
    let save_cursor = getpos(".")
    let old_query = getreg('/')
    :%s/\s\+$//e
    call setpos('.', save_cursor)
    call setreg('/', old_query)
endfunction
noremap <leader>ss :call StripWhitespace()<CR>

" Quickly save a file
nmap <leader>w :w!<CR>

" Save a file as root (,W)
noremap <leader>W :w !sudo tee % > /dev/null<CR>

" Automatic commands
if has("autocmd")

    " Enable file type detection
    " filetype on

    " Treat .json files as .js
    autocmd BufNewFile,BufRead *.json setfiletype json syntax=javascript

    " Treat .inp file as .cpp
    autocmd BufNewFile,BufRead *.inp setfiletype cpp syntax=cpp

    " Set the working directory to the current file's directory
    " autocmd BufEnter * lcd %:p:h

    " Automatically open NERDTree when vim starts with no arguments
    " autocmd vimenter * if !argc() | NERDTree | endif

    " Close vim if the only window left open is NERDTree
    autocmd bufenter * if (winnr("$") == 1 && exists("b:NERDTreeType") && b:NERDTreeType == "primary") | q | endif

endif

" Map NERDTreeToggle to F3
map <F3> :NERDTreeToggle<CR>

" Set the working directory to the active buffer's directory
set autochdir

" Make sure the working directory is set correctly
let NERDTreeChDirMode=2

" Use <leader>n to open NERDTree
nnoremap <leader>n :NERDTree .<CR>

" Map TagbarToggle to F8
nmap <F8> :TagbarToggle<CR>

" Only set mouse when using GUI, otherwise Vim may hang on startup
" due to a GPM bug (possibly related to TERM environment variable)
if has('gui_running')
    " Turn the mouse on when running the GUI
    set mouse=a
else
    set mouse=
endif

" Enable the bottom scrollbar in the GUI
set guioptions+=b

" Make regex handling perl/python compatible
nnoremap / /\v
vnoremap / /\v

" Use "jj" to exit insert mode
inoremap jj <ESC>

" Set the initial fold level for vim-markdown
let g:vim_markdown_initial_foldlevel=5

" Make window navigation easier
map <c-j> <c-w>j
map <c-k> <c-w>k
map <c-l> <c-w>l
map <c-h> <c-w>h

" Lightline configuration
let g:lightline = {
            \ 'colorscheme': 'solarized',
            \ }

" Open a new empty buffer
nmap <leader>T :enew<CR>

" Move to the next buffer
nmap <leader>l :bnext<CR>

" Move to the previous buffer
nmap <leader>h :bprevious<CR>

" Close the current buffer and move to the previous one
nmap <leader>bq :bp <BAR> bd #<CR>

" Show all open buffers and their status
nmap <leader>bl :ls<CR>

" BufExplorer
let g:bufExplorerDefaultHelp=0
let g:bufExplorerShowRelativePath=1
let g:bufExplorerFindActive=1
let g:bufExplorerSortBy='name'
map <leader>o :ToggleBufExplorer<CR>

call pathogen#helptags()
