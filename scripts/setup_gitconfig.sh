#!/bin/bash

SCRIPT_DIR="$(cd "$(dirname "${BASH_SOURCE[0]}")" && pwd)"
DOTFILES_ROOT="$(cd "${SCRIPT_DIR}/.." && pwd)"

$(git config user.name >& /dev/null && git config user.email >& /dev/null)
if [ $? -ne 0 ]; then
    echo "Please set your name and email for git" 1>&2
    exit 1
fi

authorname="$(git config user.name)"
authoremail="$(git config user.email)"
gitconfig="${DOTFILES_ROOT}/git/gitconfig"
if [ -z "$DOTFILES_ROOT" ]; then
    echo "DOTFILES_ROOT must be set" 1>&2
    exit 1
fi
if [ -z "$authorname" ]; then
    echo 'Setting user.name="'$authorname'" in '$gitconfig
    git config -f "$gitconfig" user.name "$authorname"
fi
if [ -z "$authoremail" ]; then
    echo 'Setting user.email="'$authoremail'" in '$gitconfig
    git config -f "$gitconfig" user.email "$authoremail"
fi

# source "${DOTFILES_ROOT}/proxyrc"
# echo "Setting http.proxy=$http_proxy in $gitconfig"
# git config -f "$gitconfig" http.proxy "$http_proxy"
