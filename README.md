Ben Shelley's Dotfiles
======================

This is my dotfiles repository.  There are many like it, but this one is mine.

It is a [Git][] repository that can be found at https://bitbucket.org/ben_shelley/dotfiles.

These dotfiles configure my settings just the way I like them: I use the Bash
shell with GNOME Terminal, the Vim editor, and the excellent [Solarized][] color
scheme wherever possible.  I have done my best to ensure that the settings used
are compatible with our RHEL6/7 setups here at RMS, with hopefully no dependencies
on any additional third party software that is not already installed.

[Git]: https://git-scm.com
[solarized]: http://ethanschoonover.com/solarized


Installation
------------

Clone the dotfiles repository to your home directory:

    git clone --recurse-submodules \
        https://bitbucket.org/ben_shelley/dotfiles.git \
        ~/.dotfiles
    cd ~/.dotfiles

You can "install" the dotfiles by running the `install` script, however,
there are a few important things to note before doing so:

- [dotbot][] is used to manage the install and setup of the dotfiles, using the
  `install.conf.yaml` file.

- The `install` script will create symbolic links in your home directory; if a
  link target already exists, it will not be replaced. If you would like to
  force its replacement, you can edit the `install.conf.yaml` to set the `force`
  option (see the [dotbot][] README file).

- The `install` script will overwrite your default GNOME Terminal profile.

[dotbot]: https://github.com/anishathalye/dotbot

Once you're ready, you can install by simply executing the `install` script.

    ./install

### Gnome-terminal settings for RHEL7

The `install` process will attempt to set your current Gnome-terminal profile to
use the Solarized dark colorscheme. If for some reason that process fails, you
can select the solarized color scheme yourself from the preferences menu. To do
so, open gnome-terminal and from the menu bar go to `Edit > Preferences`. In the
preferences window, select the "Default" profile on the left, and then go to the
**Colors** tab on the right. Then make the following changes:

In the **Text and Background Color** section:

- Un-check "Use colors from system theme"
- In the "Built-in schemes" drop-down menu, select "Solarized Dark"

In the **Palette** section:

- Select "Solarized" from the "Built-in schemes" drop-down menu


Features
--------

### Solarized Colors

The [Solarized][] color scheme by Ethan Schoonover is used wherever possible,
which is currently only GNOME Terminal and Vim/gVim.  The dark color scheme is
used for Vim and GNOME Terminal, while the light color scheme is used for
gVim.


### Git Settings

The `install` script will generate a custom `.gitconfig` file for you, with:

- Default editor set to `vim`
- Handy aliases: `co`, `ci`, `st`, `lg`, `unstage`, `tags`
- Default branch name for new repos is "main"


### Vim Customizations

There are too many customizations to list them all here, so I will only
highlight a few of the import ones.

- [Pathogen][]: Manage your `runtimepath` with ease.  In practical terms,
  pathogen.vim makes it super easy to install plugins and runtime files in
  their own private directories.

#### Plugins
- [BufExplorer](https://github.com/jlanzarotta/bufexplorer.git)
- [ctrlp]https://github.com/ctrlpvim/ctrlp.vim
- [lightline](https://github.com/itchyny/lightline.vim)
- [NERDTree](https://github.com/preservim/nerdtree)
- [python-syntax](https://github.com/vim-python/python-syntax.git)
- [tagbar](https://github.com/preservim/tagbar)
- [vim-colors-solarized](https://github.com/altercation/vim-colors-solarized)
- [vim-commentary](https://github.com/tpope/vim-commentary)
- [vim-json](https://github.com/elzr/vim-json)
- [vim-markdown](https://github.com/preservim/vim-markdown)
- [vim-toml](https://github.com/cespare/vim-toml.git)


### Bash Customizations

The `~/.bashrc` file will source the following files, in order, if they exist:

1. `~/.bash_exports`
2. `~/.bash_path`
3. `~/.bash_prompt`
4. `~/.bash_aliases`
5. `~/.bash_functions`
6. `~/.bash_extra`

Note that the `.bash_extra` file is not present in the repository; this is
a good place to put any of your personal customizations that you don't wish to
commit to the repository.

The `.bash_exports` script is where you should put any necessary environment
variables that need to be exported.

The `.bash_path` script currently only adds `$HOME/.local/bin` to the search
path, however, if any additional paths should be added, this file is the place
to do it.

The `.bash_prompt` script sets up the prompt the way I like it, which is
inspired by the default `cygwin` prompt.

The `.bash_aliases` script sets many useful aliases that help with things like
navigating the filesystem, providing shortcuts for frequently used commands,
colorizing `grep` and `ls` output, etc...

The `.bash_functions` script defines a few useful functions, for example, the
`path` function can be used to print your path environment variable with each
separate path component on its own line.  There are also `svndiff` and `hgdiff`
functions for viewing the output of `svn diff` or `hg diff` commands in Vim.

#### Custom Readline Init File

The `~/.inputrc` file control the behavior of the Readline library, which is
where Bash obtains its ability to allow users to edit command lines, to recall
past commands and perhaps re-edit them, and to perform history expansion on
previous commands.

My favorite feature from the `.inputrc` file is the ability to do incremental
searching with the up and down arrow keys -- just type a few characters of
a previous command, hit the up-arraw, and voila!


#### Solarized dircolors

The `~/.dircolors` file is used to apply the Solarized color scheme to the
output of `ls`.  Pretty. 


#### Custom toprc

I borrowed the `~/.toprc` file from the [CommonCHIL][] project here at RMD.

[CommonCHIL]: https://openwiki.app.ray.com/CHIL


