path_prepend "${HOME}/.local/bin"

if [ $(uname -s) == "Darwin" ]; then
    path_prepend "/usr/local/sbin"
    path_prepend "/usr/local/bin"
fi

# vim: set filetype=sh:

