# Easier navigation: .., ..., ...., ....., ~ and -
alias ..='cd ..'
alias ...='cd ../..'
alias ....='cd ../../..'
alias .....='cd ../../../..'
alias ~="cd ~" # `cd` is probably faster to type though
alias -- -="cd -"

# Shortcuts
alias dl="cd ~/Downloads"
alias dt="cd ~/Desktop"
alias g="git"
alias h="history"
alias j="jobs"
alias v="vim"

# Colorize grep output
alias grep='grep --color=auto'
alias egrep='egrep --color=auto'
alias fgrep='fgrep --color=auto'

# Start calculator with math support
alias bc='bc -l'

# Generate sha1 digest
alias sha1='openssl sha1'

# Create parent directories on demand
alias mkdir='mkdir -pv'

# Confirmation
alias mv='mv -i'
alias ln='ln -i'

alias df='df -H'
alias du='du -ch'

# Detect which `ls` flavor is in use
if ls --color > /dev/null 2>&1; then # GNU `ls`
    colorflag="--color"
else # OS X `ls`
    colorflag="-G"
fi

# List all files colorized in long format
alias l="ls -lh ${colorflag}"

# List all files colorized in long format, including dot files
alias la="ls -lha ${colorflag}"

# List only directories in short format
alias lsd='ls -d */'

# List only directories in long format
alias lsdl='ls -lh ${colorflag} | grep "^d"'

# Always use color output for `ls`
alias ls="command ls ${colorflag}"

# ROT13-encode text. Works for decoding, too! ;)
alias rot13='tr a-zA-Z n-za-mN-ZA-M'

# vim: set filetype=sh:
