#!/bin/bash

command gnome-terminal >/dev/null 2>&1
if [ $? -ne 0 ]; then
    echo "gnome-terminal not found or executed with error; skipping gnome-terminal setup"
    exit 0
fi

SCRIPT_DIR="$(cd "$(dirname "${BASH_SOURCE[0]}")" && pwd)"
DOTFILES_ROOT="$(cd "${SCRIPT_DIR}/.." && pwd)"

${SCRIPT_DIR}/gnome-terminal-colors-solarized/install.sh -s dark -p Default --skip-dircolors

